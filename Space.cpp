#include "Space.hpp"
#include <algorithm>
#include <cmath>

extern GLint attribute_v_coord, attribute_v_normal;
extern GLint uniform_m, uniform_v, uniform_p, uniform_c;
extern GLint uniform_m_3x3_inv_transp, uniform_v_inv;

void load_obj(const char* filename, Object* mesh);
glm::vec3 createVec3(double x, double y, double z);

/*******************************************
 *
 * Object
 *
 *******************************************/

Object::~Object() {
    if (vbo_vertices != 0) {
        glDeleteBuffers(1, &vbo_vertices);
    }
    if (vbo_normals != 0) {
        glDeleteBuffers(1, &vbo_normals);
    }
    if (ibo_elements != 0) {
        glDeleteBuffers(1, &ibo_elements);
    }
}

/**
 * Store object vertices, normals and/or elements in graphic card
 * buffers
 */
void Object::upload() {
    if(vertices.size() > 0) {
        glGenBuffers(1, &vbo_vertices);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]),
            vertices.data(), GL_STATIC_DRAW);
    }

    if(normals.size() > 0) {
        glGenBuffers(1, &vbo_normals);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(normals[0]),
            normals.data(), GL_STATIC_DRAW);
    }

    if(elements.size() > 0) {
        glGenBuffers(1, &ibo_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(elements[0]),
            elements.data(), GL_STATIC_DRAW);
    }
}

void Object::draw() {
    if(vbo_vertices != 0) {
        glEnableVertexAttribArray(attribute_v_coord);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);
        glVertexAttribPointer(
            attribute_v_coord,  // attribute
            4,                  // number of elements per vertex, here (x,y,z,w)
            GL_FLOAT,           // the type of each element
            GL_FALSE,           // take our values as-is
            0,                  // no extra data between each position
            0                   // offset of first element
        );
    }

    if(vbo_normals != 0) {
        glEnableVertexAttribArray(attribute_v_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
        glVertexAttribPointer(
            attribute_v_normal, // attribute
            3,                  // number of elements per vertex, here (x,y,z)
            GL_FLOAT,           // the type of each element
            GL_FALSE,           // take our values as-is
            0,                  // no extra data between each position
            0                   // offset of first element
        );
    }

    /* Apply object's transformation matrix */
    glUniformMatrix4fv(uniform_m, 1, GL_FALSE, glm::value_ptr(model));
    /* Transform normal vectors with transpose of inverse of upper left
       3x3 model matrix (ex-gl_NormalMatrix): */
    glm::mat3 m_3x3_inv_transp = glm::transpose(glm::inverse(glm::mat3(model)));
    glUniformMatrix3fv(uniform_m_3x3_inv_transp, 1, GL_FALSE, glm::value_ptr(m_3x3_inv_transp));

    //Apply color
    glUniform4fv(uniform_c, 1, glm::value_ptr(color));

    /* Push each element in buffer_vertices to the vertex shader */
    if(ibo_elements != 0) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_elements);
        int size;
        glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
        glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
    }
    else {
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    }

    if(vbo_normals != 0) {
        glDisableVertexAttribArray(attribute_v_normal);
    }
    if(vbo_vertices != 0) {
        glDisableVertexAttribArray(attribute_v_coord);
    }
}

/*******************************************
 *
 * Node
 *
 *******************************************/

 bool operator<(const Node& lhs, const Node& rhs) {
     return lhs.cost < rhs.cost;
 }

/*******************************************
 *
 * Graph
 *
 *******************************************/

std::vector<Node*> Graph::findPath(enum Pathing type, glm::vec3 start, glm::vec3 end) {
    std::vector<Node*> path;
    Node* s = new Node(start);
    Node* e = new Node(end);
    bool sf = false, ef = false;
    for(int i = 0; i < nodes.size(); i++) {
        Node* n = nodes[i];
        if(!sf && sameNode(s, n)) {
            s = n;
            sf = true;
        }
        if(!ef && sameNode(e, n)) {
            e = n;
            ef = true;
        }
    }
    if(sameNode(s, e)) {
        return path;
    }
    // Connect start and end to the graph
    if(!sf) {
        nodes.push_back(s);
    }
    if(!ef) {
        nodes.push_back(e);
    }
    std::vector<Node*> startNeighbors;
    std::vector<double> startDist; //distance of the neighbors
    std::vector<Node*> endNeighbors;
    std::vector<double> endDist;
    for(int j= 0; j < nborCount; j++) {
        startNeighbors.push_back(new Node(glm::vec3(999999, 0, 999999)));
        startDist.push_back(999999);
        endNeighbors.push_back(new Node(glm::vec3(999999, 0, 999999)));
        endDist.push_back(999999);
    }
    for(int i = 0; i < nodes.size(); i++) {
        Node* n = nodes[i];
        //start
        if(!sf) {
            double sd = glm::length(n->pos - start);
            if(sd < startDist[nborCount-1]) {
                for(int k = 0; k < nborCount; k++) {
                    if(startDist[k] > sd) { //Swap down the line
                        for(int l = nborCount-1; l >= k+1; l--) {
                            startNeighbors[l] = startNeighbors[l-1];
                            startDist[l] = startDist[l-1];
                        }
                        startNeighbors[k] = n;
                        startDist[k] = sd;
                        break;
                    }
                }
            }
        }
        //end
        if(!ef) {
            double ed = glm::length(n->pos - end);
            if(ed < endDist[nborCount-1]) {
                for(int k = 0; k < nborCount; k++) {
                    if(endDist[k] > ed) {
                        for(int l = nborCount-1; l >= k+1; l--) {
                            endNeighbors[l] = endNeighbors[l-1];
                            endDist[l] = endDist[l-1];
                        }
                        endNeighbors[k] = n;
                        endDist[k] = ed;
                        break;
                    }
                }
            }
        }
    }
    for(int i = 0; i < nborCount; i++) {
        if(startNeighbors[i]->pos.x != 999999) {
            s->conList.push_back(startNeighbors[i]);
            startNeighbors[i]->conList.push_back(s);
        }
        if(endNeighbors[i]->pos.x != 999999) {
            e->conList.push_back(endNeighbors[i]);
            endNeighbors[i]->conList.push_back(e);
        }
    }
     
    switch(type) {
        case DFS:
            path = pathDFS(s, path, s, e);
            break;
        case BFS:
            path = pathBFS(s, e);
            break;
        case UCS:
            path = pathUCS(s, e);
            break;
        case GBFS:
            path = pathGBFS(s, e);
            break;
        case ASTAR:
            path = pathAStar(s, e);
            break;
     }
     // Remove start and end from graph
     return path;
 }



std::vector<Node*> Graph::pathDFS(Node* node, std::vector<Node*> path, Node* start, Node* end) {
    if(sameNode(node, end)) { //Goal found. Begin recursive fallback
        std::vector<Node*> g;
        g.push_back(node);
        return g;
    }
    std::vector<Node*> children = node->conList;
    for(int i = 0; i < children.size(); i++) {
        path = pathDFS(children[i], path, start, end);
        if(path.size() != 0) {
            path.insert(path.begin(), node);
            return path;
        }
    }

    std::vector<Node*> empty;
    return empty;
}

std::vector<Node*> Graph::pathBFS(Node* start, Node* end) {
    std::deque<Node*> closedList;
    std::deque<Node*> queue;
    int count = 0;

    queue.push_back(start);
    closedList.push_back(start);
    start->parent = NULL;
    while(!closedList.empty()) {
        Node* next = queue.front();
        queue.pop_front();
        std::vector<Node*> edges = next->conList;
        for(int i = 0; i < edges.size(); i++) {
            Node* edge = edges[i];
            count++;
            if(sameNode(edge, end)) {
                edge->parent = next;
                closedList.push_back(edge);
                printf("BFS Expanded Nodes: %d\n", count);
                return makePath(end);
            }
            if(!contains(edge, closedList)) {
                edge->parent = next;
                closedList.push_back(edge);
                queue.push_back(edge);
            }
        }
    }
    std::vector<Node*> e;
    return e;
}

std::vector<Node*> Graph::pathUCS(Node* start, Node* end) {
    std::deque<Node*> closedList;
    std::vector<Node*> queue;
    int count = 0;

    start->cost = 0;
    start->parent = NULL;
    queue.push_back(start);
    std::push_heap(queue.begin(), queue.end(), CmpNodes());
    closedList.push_back(start);
    while(!queue.empty()) {
        std::pop_heap(queue.begin(), queue.end(), CmpNodes());
        Node* next = queue.back();
        queue.pop_back();
        count++;
        if(sameNode(next, end)) {
            printf("UCS Expanded Nodes: %d\n", count);
            return makePath(end);
        }
        std::vector<Node*> edges = next->conList;
        for(int i = 0; i < edges.size(); i++) {
            Node* edge = edges[i];
            double c = getGCost(edge, next);
            if(!contains(edge, closedList)) {
                edge->parent = next;
                closedList.push_back(edge);
                edge->cost = c;
                queue.push_back(edge);
                std::push_heap(queue.begin(), queue.end(), CmpNodes());
            }
            else {
                //Find node and update cost if necessary
                for(int i = 0; i < queue.size(); i++) {
                    if(sameNode(edge, queue[i]) && queue[i]->cost > c) {
                        queue[i]->cost = c;
                        queue[i]->parent = next;
                        std::make_heap(queue.begin(), queue.end(), CmpNodes());
                        break;
                    }
                }
            }
        }
    }
    std::vector<Node*> e;
    return e;
}

std::vector<Node*> Graph::pathGBFS(Node* start, Node* end) {
    std::deque<Node*> closedList;
    std::vector<Node*> queue;
    int count = 0;

    start->cost = 0;
    start->parent = NULL;
    queue.push_back(start);
    std::push_heap(queue.begin(), queue.end(), CmpNodes());
    closedList.push_back(start);
    while(!queue.empty()) {
        std::pop_heap(queue.begin(), queue.end(), CmpNodes());
        Node* next = queue.back();
        queue.pop_back();
        count++;
        if(sameNode(next, end)) {
            printf("GBFS Expanded Nodes: %d\n", count);
            return makePath(end);
        }
        std::vector<Node*> edges = next->conList;
        for(int i = 0; i < edges.size(); i++) {
            Node* edge = edges[i];
            double c = getHCost(edge, end);
            if(!contains(edge, closedList)) {
                edge->parent = next;
                closedList.push_back(edge);
                edge->cost = c;
                queue.push_back(edge);
                std::push_heap(queue.begin(), queue.end(), CmpNodes());
            }
            else {
                //Find node and update cost if necessary
                for(int i = 0; i < queue.size(); i++) {
                    if(sameNode(edge, queue[i]) && queue[i]->cost > c) {
                        queue[i]->cost = c;
                        queue[i]->parent = next;
                        std::make_heap(queue.begin(), queue.end(), CmpNodes());
                        break;
                    }
                }
            }
        }
    }
    std::vector<Node*> e;
    return e;
}

std::vector<Node*> Graph::pathAStar(Node* start, Node* end) {
    std::deque<Node*> closedList;
    std::vector<Node*> queue;
    int count = 0;

    start->cost = 0;
    start->parent = NULL;
    queue.push_back(start);
    std::push_heap(queue.begin(), queue.end(), CmpNodes());
    closedList.push_back(start);
    while(!queue.empty()) {
        std::pop_heap(queue.begin(), queue.end(), CmpNodes());
        Node* next = queue.back();
        queue.pop_back();
        count++;
        if(sameNode(next, end)) {
            printf("A* Expanded Nodes: %d\n", count);
            return makePath(end);
        }
        std::vector<Node*> edges = next->conList;
        for(int i = 0; i < edges.size(); i++) {
            Node* edge = edges[i];
            double c = getGCost(edge, next) + getHCost(edge, end);
            if(!contains(edge, closedList)) {
                edge->parent = next;
                closedList.push_back(edge);
                edge->cost = c;
                queue.push_back(edge);
                std::push_heap(queue.begin(), queue.end(), CmpNodes());
            }
            else {
                //Find node and update cost if necessary
                for(int i = 0; i < queue.size(); i++) {
                    if(sameNode(edge, queue[i]) && queue[i]->cost > c) {
                        queue[i]->cost = c;
                        queue[i]->parent = next;
                        std::make_heap(queue.begin(), queue.end(), CmpNodes());
                        break;
                    }
                }
            }
        }
    }
    std::vector<Node*> e;
    return e;
}

bool Graph::sameNode(Node* n1, Node* n2) {
    return glm::length(n1->pos - n2->pos) < 0.0001;
}

double Graph::getGCost(Node* n, Node* m) {
    //find the cost of the last link, then sum the previous
    glm::vec3 dist = n->pos - m->pos;
    double cost = glm::length(dist);
    if(m->parent != NULL) {
        cost += m->parent->cost;
    }
    return cost;
}

double Graph::getHCost(Node* n, Node* end) {
    glm::vec3 dist = end->pos - n->pos;
    return glm::length(dist);
}

std::vector<Node*> Graph::makePath(Node* end) {
    std::vector<Node*> path;
    path.push_back(end);
    Node* next = end->parent;
    while(next != NULL) {
        path.push_back(next);
        next = next->parent;
    }
    std::reverse(path.begin(), path.end());
    return path;
}

bool Graph::contains(Node* n, std::deque<Node*> closedList) {
    for(int i = 0; i < closedList.size(); i++) {
        if(sameNode(n, closedList[i])) {
            return true;
        }
    }
    return false;
}

/*******************************************
 *
 * CSpace
 *
 *******************************************/

CSpace::CSpace() {
    resolution = glm::vec2(0);
    nodeCount = 0;
    nborCount = 0;
    pos = glm::vec3(0);
    wh = glm::vec2(0);
    cspace = NULL;
}

CSpace::CSpace(glm::vec2 res) {
    resolution = res;
    nodeCount = 500;
    nborCount = 6;
    pos = glm::vec3(0);
    wh = glm::vec2(0);
    cspace = new int*[(int)resolution.x];
    for(int i = 0; i < (int)resolution.x; i++) {
        cspace[i] = new int[(int)resolution.y];
    }
}

CSpace::CSpace(glm::vec2 res, int count) {
    resolution = res;
    nodeCount = count;
    pos = glm::vec3(0);
    wh = glm::vec2(0);
    cspace = new int*[(int)resolution.x];
    for(int i = 0; i < (int)resolution.x; i++) {
        cspace[i] = new int[(int)resolution.y];
    }
}

CSpace::CSpace(glm::vec2 res, int count, int neighborCount) {
    resolution = res;
    nodeCount = count;
    nborCount = neighborCount;
    pos = glm::vec3(0);
    wh = glm::vec2(0);
    cspace = new int*[(int)resolution.x];
    for(int i = 0; i < (int)resolution.x; i++) {
        cspace[i] = new int[(int)resolution.y];
    }
}

Graph CSpace::getGraphPRM() {
    if(cspace == NULL) {
        return Graph();
    }
    //PRM
    Graph g;
    g.nborCount = nborCount;
    //Make list of random nodes
    for(int i = 0; i < nodeCount; i++) {
        double randX = (double)rand()/RAND_MAX * wh.x;
        //Normally would include a + pos.x, but it was ignored here since the index formula would subtract it by pos.x
        double randY = (double)rand()/RAND_MAX * wh.y;
        int xIndex = (int)std::floor(randX / (wh.x/resolution.x));
        int yIndex = (int)std::floor(randY / (wh.y/resolution.y));
        if(!cspace[xIndex][yIndex]) { // 0 is valid space
            g.nodes.push_back(new Node(glm::vec3(randX+pos.x, 0, randY+pos.z)));
        }
        // else - the node is in an object
    }

    //For each node, find the k nearest neighbors
    //const int nNum = 6; //Number of neighbors to search for
    for(int i = 0; i < g.nodes.size(); i++) {
        std::vector<Node*> neighbors;
        std::vector<double> nDist; //distance of the neighbors
        for(int j= 0; j < nborCount; j++) {
            neighbors.push_back(new Node(glm::vec3(999999, 0, 999999)));
            nDist.push_back(999999);
        }
        Node* n = g.nodes[i];
        //Search through every other node
        int index = 0;
        for(int j = 0; j < g.nodes.size(); j++) {
            if(i == j) {
                continue;
            }
            //Check if new node is closer and add appropriately
            Node* m = g.nodes[j];
            double dist = glm::length(n->pos - m->pos);
            for(int k = 0; k < nborCount; k++) {
                if(nDist[k] > dist) { //Swap down the line
                    for(int l = nborCount-1; l >= k+1; l--) {
                        neighbors[l] = neighbors[l-1];
                        nDist[l] = nDist[l-1];
                    }
                    neighbors[k] = m;
                    nDist[k] = dist;
                    break;
                }
            }

        } //End searching through every other node
        //n->conList = neighbors;
        std::vector<Node*>::iterator it;
        for(it = neighbors.begin(); it != neighbors.end();) {
            Node* m = *it;
            //glm::vec3 path = m->pos - n->pos;
            //Find out if path is clear
            if(isPathClear(n->pos.x, n->pos.z, m->pos.x, m->pos.z)) {
                //add node to connection list
                n->conList.push_back(m);
                //If path is clear and n is not a part of the connectivity list of the neighbors, add it
                std::vector<Node*> nlist = m->conList;
                bool exists = false;
                for(int k = 0; k < nlist.size(); k++) {
                    if(std::abs(glm::length(n->pos - nlist[k]->pos)) < 0.00001) {
                        exists = true;
                        break;
                    }
                }
                if(!exists) {
                    m->conList.push_back(n);
                }
                it++;
            }
            else { //obstacle in the way, delete node
                it = neighbors.erase(it);
            }
        }
    }
    return g;
}

bool CSpace::isPathClear(double x0, double y0, double x1, double y1) {
    //Get the line between them (vector)
    glm::vec2 v = glm::vec2(x1, y1) - glm::vec2(x0, y0);
    double dist = 0;        //current distance along vector
    double step = 0.01;   //step size along vector (fraction of vector to go)
    glm::vec2 c(x0, y0);
    while(dist <= 1.0) { //March along vector
        int xIndex = (int)std::floor((c.x-pos.x) / (wh.x/resolution.x));
        int yIndex = (int)std::floor((c.y-pos.z) / (wh.y/resolution.y));
        if(cspace[xIndex][yIndex]) { //if index is not valid space, return false
            return false;
        }
        dist += step;
        c.x += step*v.x;
        c.y += step*v.y;
    }

    return true;
}

void CSpace::tightenPath(std::vector<Node*> &path) {
    if(path.size() < 3) {
        return; //can't smooth
    }
    std::vector<Node*> newpath;
    newpath.push_back(path[0]);
    int i1 = 0; //start of line
    int i2 = 2; //end of line
    while(i2 < path.size()) {
        while(i2 < path.size() && isPathClear(path[i1]->pos.x, path[i1]->pos.z, path[i2]->pos.x, path[i2]->pos.z)) {
            i2++;
        }
        if(i2 != path.size()) {
            newpath.push_back(path[i2-1]);
            i1 = i2-1;
            i2 = i2 + 1;
        }
    }
    newpath.push_back(path[path.size()-1]); //add last node to path
    path = newpath;
}

/*******************************************
 *
 * Agent
 *
 *******************************************/

void Agent::moveAgent(CSpace c, double dt, std::vector<Agent*> agents, int thisAgent, std::vector<Object*> obst) {
    //printf("Agent %d before: (%f, %f, %f)\n", thisAgent, agent->pos.x, agent->pos.y, agent->pos.z);
    if(path.size() > 1 && pathIndex < path.size()) {
        //If the next node is clear, move straight to that one
        if(smooth && pathIndex+1 < path.size()) {
            //if(agent->pos.x != agent->pos.x || isinf(agent->pos.x)) {
            //    printf("nan: %d\n", thisAgent);
            //}
            if(c.isPathClear(agent->pos.x, agent->pos.z, path[pathIndex+1]->pos.x, path[pathIndex+1]->pos.z)) {
                pathIndex++;
            }
        }
        Node* curNode = path[pathIndex];
        glm::vec3 p2Node = curNode->pos - agent->pos;
        glm::vec3 np2Node = glm::normalize(p2Node);
        glm::vec3 goalF = speed * np2Node;
        if(glm::length(p2Node) < 0.05) { //Move agent along the path
            pathIndex++;
            //printf("Index: %d/%d\n", pathIndex, path.size()-1);
        }
        
        ttc(agents, thisAgent, obst);
        //printf("goal, avoid: (%f, %f, %f), (%f, %f, %f)\n", goalF.x, goalF.y, goalF.z, avoidF.x, avoidF.y, avoidF.z);
        glm::vec3 force = goalF + avoidF;
        glm::vec3 v = force * (float)dt;
        
        agent->pos += (float)dt * vel;
        agent->model = glm::translate(agent->model, (float)dt * vel);
        vel += v;
        if(glm::length(vel) > speed) {
            vel = speed * (vel / glm::length(vel));
        }
    }
    //printf("Agent %d after: (%f, %f, %f)\n", thisAgent, agent->pos.x, agent->pos.y, agent->pos.z);
} 

void Agent::ttc(std::vector<Agent*> agents, int thisAgent, std::vector<Object*> obst) {
    const float maxt = 999;
    float k = 1.5, m = 2.0, t0 = 3, maxF = 7.0f;
    float t = maxt;
    //Check for collision against agents
    for(int i = 0; i < agents.size(); i++) {
        if(i != thisAgent && notVisited(pastAgents, i)) {
            Agent* ag = agents[i];
            glm::vec3 relPos = ag->agent->pos - agent->pos;
            glm::vec3 relVel = vel - ag->vel;
            
            float a = glm::dot(relVel, relVel);
            float b = glm::dot(relPos, relVel);
            float c = glm::dot(relPos, relPos) - std::pow((agent->radius + ag->agent->radius), 2);
            float det = b*b - a*c;
            if(det < 0 || (a < 0.001 && a > -0.001)) {
                continue;
            }
            det = std::sqrt(det);
            t = (b - det) / a;
            if(t < 0 || t > maxt) {
                continue;
            }
            
            glm::vec3 favoid = k*std::exp(-t/t0)*(relVel - (relVel*b - relPos*a)/det)/(a*std::pow(t, m))*(m/t + 1.0f/t0);
            favoid = -favoid;
            float mag = glm::length(favoid);
            if(mag > maxF) {
                favoid = maxF * favoid / mag;
            }
            avoidF += favoid;
            ag->avoidF -= favoid;
            ag->pastAgents.push_back(thisAgent);
        }
    }
    //Check for collision against obstacles
    for(int i = 0; i < obst.size(); i++) {
        Object* ob = obst[i];
        glm::vec3 relPos = ob->pos - agent->pos;
        glm::vec3 relVel = vel;
        
        float a = glm::dot(relVel, relVel);
        float b = glm::dot(relPos, relVel);
        float c = glm::dot(relPos, relPos) - std::pow((agent->radius + ob->radius), 2);
        float det = b*b - a*c;
        if(det < 0 || (a < 0.001 && a > -0.001)) {
            continue;
        }
        det = std::sqrt(det);
        t = (b - det) / a;
        if(t < 0 || t > maxt) {
            continue;
        }
        
        glm::vec3 favoid = k*std::exp(-t/t0)*(relVel - (relVel*b - relPos*a)/det)/(a*std::pow(t, m))*(m/t + 1.0f/t0);
        favoid = -favoid;
        float mag = glm::length(favoid);
        if(mag > maxF) {
            favoid = maxF * favoid / mag;
        }
        avoidF += favoid;
    }
}

bool Agent::notVisited(std::vector<int> past, int index) {
    for(int i = 0; i < past.size(); i++) {
        if(past[i] == index) {
            return false;
        }
    }
    return true;
}

void Agent::draw() {
    agent->draw();
}

void Agent::upload() {
    agent->upload();
}

/*******************************************
 *
 * Space
 *
 *******************************************/

void Space::upload() {
    for(int i = 0; i < objects.size(); i++) {
        objects[i]->upload();
    }
    for(int i = 0; i < obstacles.size(); i++) {
        obstacles[i]->upload();
    }
    for(int i = 0; i < agents.size(); i++) {
        agents[i]->upload();
    }
}

void Space::draw() {
    for(int i = 0; i < objects.size(); i++) {
        if(objects[i]->name != "node") {
            objects[i]->draw();
        }
    }
    for(int i = 0; i < obstacles.size(); i++) {
        obstacles[i]->draw();
    }
    for(int i = 0; i < agents.size(); i++) {
        agents[i]->draw();
    }
}

void Space::drawNodes(std::vector<Node*> allNodes, bool drawNodes, bool drawConnection) {
    if(connection == NULL) {
        //load connection object
        connection = new Object();
        load_obj("models/connection.obj", connection);
        connection->name = "connector";
        connection->color = glm::vec4(0.0, 1.0, 0.0, 1.0);
        connection->upload();
    }
    if(drawNodes) {
        for(int i = 0; i < allNodes.size(); i++) {
            Node* n = allNodes[i];
            node->model = glm::mat4(1.0);
            node->model = glm::translate(node->model, n->pos);
            node->model = glm::translate(node->model, glm::vec3(0, 0.5, 0));
            node->model = glm::scale(node->model, glm::vec3(0.2, 0.2, 0.2));
            bool found = false;
            for(int j = 0; j < agents.size(); j++) {
                std::vector<Node*> pathNodes = agents[j]->path;
                if(pathNodes.size() > 0 && (glm::length(n->pos - pathNodes[0]->pos) < 0.0001 || glm::length(n->pos - pathNodes[pathNodes.size()-1]->pos) < 0.0001)) {
                    node->color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
                    found = true;
                    break;
                }
            }
            if(!found) {
                node->color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
            }
            node->draw();
            if(drawConnection) {    //draw all node connections
                std::vector<Node*> nlist = n->conList;
                for(int j = 0; j < nlist.size(); j++) {
                    connection->model = glm::mat4(1.0);
                    Node* m = nlist[j];
                    glm::vec3 line = m->pos - n->pos;
                    double len = glm::length(line);
                    //translate
                    glm::vec3 hp = n->pos + glm::vec3(line.x/2.0, 0.25, line.z/2.0); //find the halfway point
                    connection->model = glm::translate(connection->model, hp);
                    //rotate
                    glm::vec3 nLine = glm::normalize(line);
                    float angle = acos(glm::dot(glm::vec3(-1.0, 0.0, 0.0), nLine));
                        //Get direction the angle is in (u1v2 - u2v1)
                        //This ends up as 0-(-1*nLine.z), so it's just nLine.z
                    int dir = 1;
                    if(nLine.z < 0) {
                        dir = -1;
                    }
                    connection->model = glm::rotate(connection->model, dir*angle+(3.1415926f/2.0f), glm::vec3(0.0, 1.0, 0.0));
                    //scale
                    connection->model = glm::scale(connection->model, glm::vec3(0.04, 0.04, len/2.0));

                    connection->draw();
                }
            }
        }
    }
    //Draw path
    for(int i = 0; i < agents.size(); i++) {
        std::vector<Node*> pathNodes = agents[i]->path;
        if(!drawConnection && pathNodes.size() > 1) {
            for(int i = 0; i < pathNodes.size()-1; i++) {
                Node* n = pathNodes[i];
                connection->model = glm::mat4(1.0);
                Node* m = pathNodes[i+1];
                glm::vec3 line = m->pos - n->pos;
                double len = glm::length(line);
                //translate
                glm::vec3 hp = n->pos + glm::vec3(line.x/2.0, 0.25, line.z/2.0); //find the halfway point
                connection->model = glm::translate(connection->model, hp);
                //rotate
                glm::vec3 nLine = glm::normalize(line);
                float angle = acos(glm::dot(glm::vec3(-1.0, 0.0, 0.0), nLine));
                    //Get direction the angle is in (u1v2 - u2v1)
                    //This ends up as 0-(-1*nLine.z), so it's just nLine.z
                int dir = 1;
                if(nLine.z < 0) {
                    dir = -1;
                }
                connection->model = glm::rotate(connection->model, dir*angle+(3.1415926f/2.0f), glm::vec3(0.0, 1.0, 0.0));
                //scale
                connection->model = glm::scale(connection->model, glm::vec3(0.04, 0.04, len/2.0));

                connection->draw();
            }
        }
    }
}

CSpace Space::getCSpace() {
    CSpace cs(gridResolution, nodeCount, nborCount);

    //Find the bounding volume
    glm::vec4 min = agents[0]->agent->vertices[0];
    min.y = 0; //2D
    glm::vec4 max = min;
    for(int i = 0; i < obstacles.size(); i++) {
        std::vector<glm::vec4> verts = obstacles[i]->vertices;
        for(int j = 0; j < verts.size(); j++) {
            verts[j] = obstacles[i]->model * verts[j]; //Put into world space
            if(verts[j].x < min.x) {
                min.x = verts[j].x;
            }
            if(verts[j].z < min.z) {
                min.z = verts[j].z;
            }
            if(verts[j].x > max.x) {
                max.x = verts[j].x;
            }
            if(verts[j].z > max.z) {
                max.z = verts[j].z;
            }
        }
    }
    for(int i = 0; i < objects.size(); i++) {
        std::vector<glm::vec4> verts = objects[i]->vertices;
        for(int j = 0; j < verts.size(); j++) {
            verts[j] = objects[i]->model * verts[j]; //Put into world space
            if(verts[j].x < min.x) {
                min.x = verts[j].x;
            }
            if(verts[j].z < min.z) {
                min.z = verts[j].z;
            }
            if(verts[j].x > max.x) {
                max.x = verts[j].x;
            }
            if(verts[j].z > max.z) {
                max.z = verts[j].z;
            }
        }
    }

    cs.pos = glm::vec3(min);
    cs.wh = glm::vec2(max.x-min.x, max.z-min.z);

    //Figure out the valid space
    for(int i = 0; i < cs.resolution.x; i++) {
        for(int j = 0; j < cs.resolution.y; j++) {
            //Get the point in the middle of the cell
            glm::vec3 point = glm::vec3(i*(cs.wh.x/cs.resolution.x)+min.x+(cs.wh.x/cs.resolution.x)/2, 0.0, j*(cs.wh.y/cs.resolution.y)+min.z+(cs.wh.y/cs.resolution.y)/2);
            //Do a point in polygon test to test if it's valid or not
                //Since we're working with cylinders, we can just use a point in circle test
            cs.cspace[i][j] = 0; //Mark initially as free
            for(int k = 0; k < obstacles.size(); k++) {
                if(glm::length(point - obstacles[k]->pos) < (agents[0]->agent->radius + obstacles[k]->radius)) {
                    cs.cspace[i][j] = 1;
                }
            }
        }
    }
    #ifdef DEBUG
    printf("Configuration Space\n==========================\n");
    printf("Min: %f, %f, %f\n", min.x, min.y, min.z);
    printf("Max: %f, %f, %f\n", max.x, max.y, max.z);
    for(int i = 0; i < (int)cs.resolution.x; i++) {
        for(int j = 0; j < (int)cs.resolution.y; j++) {
            printf("%d ", cs.cspace[i][j]);
        }
        printf("\n");
    }
    #endif
    return cs;
}

void Space::moveAgents(CSpace c, double dt) {
    for(int i = 0; i < agents.size(); i++) {
        agents[i]->pastAgents.clear();
        agents[i]->avoidF = glm::vec3(0);
    }
    for(int i = 0; i < agents.size(); i++) {
        agents[i]->moveAgent(c, dt, agents, i, obstacles);
    }
}
