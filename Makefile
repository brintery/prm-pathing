CXXFLAGS=-ggdb
LDLIBS=-lglut -lGLEW -lGL -lGLU -lm
all: path path2 path3 path4
path: Space.o shader_utils.o
path2: Space.o shader_utils.o
path3: Space.o shader_utils.o
path4: Space.o shader_utils.o
clean:
	rm -f *.o path

.PHONY: all clean
